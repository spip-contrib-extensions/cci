# Copier Coller Image - CCI

![CCI](prive/themes/spip/images/cci-xx.svg)

Permet de copier des images dans le texte lors de l'édition d'un objet éditoriale. Une image collée est jointe comme un document et
la copie produit du code de format `<docxx|>` dans le texte sur le curseur. Où xx est l'identifiant de l'image inséré.


**Documentation officielle**\
https://contrib.spip.net/5452

