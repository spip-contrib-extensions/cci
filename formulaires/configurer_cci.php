<?php
if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies
 **/
function formulaires_configurer_cci_saisies_dist()
{
	$saisies = array(
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'conf_modele_cci',
				'label' => _T('cci:label_choix_model'),
				'data' => array(
					'doc' => _T('cci:choix_modele_doc'),
					'img' => _T('cci:choix_modele_img'),
				),
				'defaut' => 'doc',
			),
		),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'conf_alignement_modele_cci',
				'label' => _T('cci:label_alignement'),
				'data' => array(
					'left' => 'Left',
					'right' => 'Right',
					'center' => 'Center',
				),
				'defaut' => 'left',
			),
		),
		array(
			'saisie' => 'hidden',
			'options' => array(
				'nom' => '_meta_casier',
				'defaut' => 'conf_modele_cci',
			),
		)
	);
	return $saisies;
}
