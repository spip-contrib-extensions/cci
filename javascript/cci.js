
window.addEventListener('paste', e => {
	if (e.clipboardData && e.clipboardData.files.length == 0) return

	var lien = '';
	const activeElement = document.activeElement
	if (activeElement.tagName != 'TEXTAREA' || !activeElement.hasAttribute('ccilien')) {
		var dataH = document.querySelector('.cci_data_crayon');
		if (!dataH) return;
		lien = dataH.getAttribute('lien');
		if (!lien) return;
	} else {
		lien = activeElement.getAttribute('ccilien');
	}

	const data = new FormData();
	var hasTypeImage = false
	for (var i = 0; i < e.clipboardData.files.length; i++) {
		if (e.clipboardData.files[i].type.indexOf("image") !== -1) {
			hasTypeImage = true
		}
		var extension = e.clipboardData.files[i].type.replace(/(.*)\//g, '')
		data.append('fichiers[' + i + ']', e.clipboardData.files[i], Date.now() + '.' + extension);
	}

	if (!hasTypeImage) return
	e.preventDefault()

	const options = {
		method: 'POST',
		body: data,
	};

	fetch(lien.replaceAll('&amp;', '&'), options)
		.then(response => response.json())
		.then(json => {
			cci_inserer_sur_curseur(activeElement, json.data)
			if ($('#navigation > div.ajaxbloc').length > 0) {
				$('#navigation > div.ajaxbloc').first().ajaxReload()
			}
			if (typeof e.clipboardData.clearData == "function") {
				e.clipboardData.clearData()
			}
		});

});

function cci_inserer_sur_curseur(myField, myValue) {
	//IE support
	if (document.selection) {
		myField.focus();
		sel = document.selection.createRange();
		sel.text = myValue;
	}
	//MOZILLA and others
	else if (myField.selectionStart || myField.selectionStart == '0') {
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		myField.value = myField.value.substring(0, startPos)
			+ myValue
			+ myField.value.substring(endPos, myField.value.length);
	} else {
		myField.value += myValue;
	}
}