<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2020                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/getdocument');
include_spip('inc/documents');
function action_ajouter_imagecci_dist($arg = null)
{

	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	if (is_null($arg) || !is_string($arg) || !isset($_FILES['fichiers']) || !is_array($_FILES['fichiers'])) {
		return array(
			"succes" => false,
			"data" => "argument invalide ou action non autorisé ou fichier introuvable",
		);
	}

	$arg = explode('-', $arg);
	if (count($arg) != 2) {
		return array(
			"succes" => false,
			"data" => "Argument d'objet invalide",
		);
	}

	$resultat = array(
		'data' => '',
	);
	include_spip('action/ajouter_documents');
	include_spip('inc/rechercher');
	$ajouter_un_document = charger_fonction('ajouter_un_document', 'action');
	$default_conf = lire_config('conf_modele_cci/conf_modele_cci', 'doc');
	$align = lire_config('conf_modele_cci/conf_alignement_modele_cci', 'left');
	foreach ($_FILES['fichiers']['name'] as $index => $name) {
		// on remplace le nom par le md5 du fichier
		$md5name = md5_file($_FILES['fichiers']['tmp_name'][$index]);
		$name = preg_replace('/^(.+)(\.\w+)$/', $md5name . '$2', $name);

		$retour = '';
		$idDoc = '';
		// vérifier si le document n'existe pas déjà et utiliser l'identifiant
		// du document existant au lieu de créer un nouveau document
		$resDocumentExistant = recherche_en_base($name, 'document');
		if (count($resDocumentExistant['document']) == 1) {
			$idDoc = array_keys($resDocumentExistant['document'])[0];
		}

		// document à ajouter
		if (!is_numeric($idDoc)) {
			// reconstruction d'un fichier singulier de type $_FILE
			$file = array();
			$file['name'] = $name;
			$file['type'] = $_FILES['fichiers']['type'][$index];
			$file['tmp_name'] = $_FILES['fichiers']['tmp_name'][$index];
			$file['error'] = $_FILES['fichiers']['error'][$index];
			$file['size'] = $_FILES['fichiers']['size'][$index];
			// surcharge pour spip
			$file['titrer'] = false;
			$file['mode'] = 'image';
			$idDoc = $ajouter_un_document('new', $file, $arg[0], $arg[1], "image");
		}

		if (is_numeric($idDoc)) {
			$retour = "<" . $default_conf . "$idDoc|$align>";
		} else {
			$retour = "[cci] Erreur $idDoc";
		}
		$resultat['data'] .= $retour;
	}

	echo json_encode($resultat);
}
