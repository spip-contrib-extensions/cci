<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2020                                                *
 *  rija.propitech														   *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

/**
 * Utilisations de pipelines
 *
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Ajout du script js dans le header de l'espace privé 
 **/
function cci_header_prive($flux)
{
	$js = find_in_path('javascript/cci.js');
	$flux .= "\n<script src=\"$js\" type=\"text/javascript\"></script>\n";
	return $flux;
}

/**
 * Ajouter le script dans l'espace public et tenir compte du plugin crayon s'il est activé
 **/
function cci_insert_head($flux)
{
	if (test_plugin_actif('crayons') == true && isset($GLOBALS['contexte']['type-page'])) {
		$objet = $GLOBALS['contexte']['type-page'];
		$id_objet = @$GLOBALS['contexte']['id_' . $objet];
		// si valide, on ajoute un input hidden contenant le lien pour l'édition de l'objet
		if (is_numeric($id_objet) && in_array($objet, ['article', 'rubrique']) && cci_si_autoriser($objet, $id_objet)) {
			$flux .= "<input type='hidden' class='cci_data_crayon' lien='" . htmlentities(cci_lien_edition($objet, $id_objet)) . "'/>";
		}
	}

	$js = find_in_path('javascript/cci.js');
	$flux .= "\n<script src=\"$js\" type=\"text/javascript\"></script>\n";
	return $flux;
}

/**
 * Insertion d'attribut de nom 'ccilien' sur les textarea d'édition des objets
 **/
function cci_formulaire_fond($fond)
{

	if (
		isset($fond['args']['form'])
		&& in_array($fond['args']['form'], ['forum', 'forum_prive', 'editer_article', 'editer_rubrique', 'editer_breve'])
	) {

		$objets = ['forum', 'forum_prive', 'article', 'rubrique', 'breve'];
		$objet = '';
		$id_objet = '';

		if (in_array($fond['args']['form'], ['forum', 'forum_prive'])) {
			$objet = $fond['args']['form'];
			$id_objet = $fond['args']['contexte']['id_forum'] ?? "";
		} else if (strpos($fond['args']['form'], 'editer_') !== false) {
			$objet = explode('_', $fond['args']['form'])[1];
			$id_objet = $fond['args']['contexte']["id_$objet"];
		}

		if (empty($objet) || empty($id_objet)) return $fond;

		// détermination de l'autorisation, si c'est un objet à créer. L'identifiant est 'oui' et on utilise un identifiant égale à -1
		// pour voir l'autorisation
		$id_objet_autorisation = !is_numeric($id_objet) ? -1 : $id_objet;
		if (!cci_si_autoriser($objet, $id_objet_autorisation)) return $fond;

		// ajout de l'attribut ccilien aux textarea
		$lien = cci_lien_edition($objet, $id_objet);
		$html = preg_replace('/\<textarea(.*)\>/Uims', '<textarea ccilien="' . $lien . '" $1>', $fond['data']);

		if ($html !== false) {
			$fond['data'] = $html;
		}
	}

	return $fond;
}


/**
 * Détermine si on est autorisé à joindre un document par cci pour cet objet
 *
 * @param string $objet
 *     Type de l'objet
 * @param string $id_objet
 *     identifiant de l'objet
 * @return bool
 *
 **/
function cci_si_autoriser($objet, $id_objet)
{
	include_spip('inc/autoriser');
	$autoriser = false;
	if (isset($GLOBALS['visiteur_session']['id_auteur']) && in_array($objet, ['forum', 'forum_prive'])) {
		$autoriser = true;
	} else {
		$autoriser = autoriser('joindredocument', $objet, $id_objet);
	}
	return $autoriser;
}

/**
 * Produit un lien sécuriser pour l'édition d'un objet
 *
 * @param string $objet
 *     Type de l'objet
 * @param string $id_objet
 *     identifiant de l'objet
 * @return string
 *     le lien
 **/
function cci_lien_edition($objet, $id_objet)
{
	$securiser_action = charger_fonction('securiser_action', 'inc');
	return $securiser_action('ajouter_imagecci', "$objet-$id_objet", "", false);
}
