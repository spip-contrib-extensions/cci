<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'cci_description' => 'Permet de copier-coller des images dans site SPIP.',
	'cci_nom' => 'Copier Coller Image',
	'cci_slogan' => 'Copier-coller les images dans du contenu rédactionnel',
);