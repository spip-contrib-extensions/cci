<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'configurer' => 'Configurer Copier Coller Image',
	'label_choix_model' => 'Pour l\'insertion de l\'image',
	'choix_modele_doc'	=> 'Modèle doc',
	'choix_modele_img'  => 'Modèle img',
	'label_alignement'  => 'Alignement par défaut',
);
