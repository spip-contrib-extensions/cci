# CHANGELOG

## [1.1.3] - 2023-06-06


### Corrections de bugs

- Correction du copier-coller d'image qui ne marche lors de la création d'un objet éditoriale
- Correction de l'ajout de la même image qui crée différents documents
- Correction du paramètre d'alignement non pris en compte

### Améliorations

- Réfactorisation du formulaire de configuration pour être conforme aux normes de SPIP
- Formatage de code source



